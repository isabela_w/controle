// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
      apiKey: 'AIzaSyBUAuL5KwhXQHnBPeUBl25RMrajW2F7DR4',
      authDomain:'controle-isawurth.firebaseapp.com',
      projectId: 'controle-isawurth',
      storageBucket: 'controle-isawurth.appspot.com',
      messagingSenderId: '140819979419',
      appId: '1:140819979419:web:a64300ee89fa4ac96c3a3e',
      measurementId: 'G-L8B0FB6ESH'
    }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
